
var w = $(window).width(), //横
    h = $(window).height(); //縦

//それぞれのファイル
var I_file,twitter_file,others_file;
//var read_file = "ranking_fix.csv";
//URLのパラメタを取得
var user = getUrlVars()["user"];
console.log(user);

if(user == undefined){
	I_file = "data/I_kupoyuki.csv"
	twitter_file = "data/twi_kupoyuki.csv";
	others_file = "data/others_kupoyuki.csv";
}else{
	I_file = "data/I.csv";
	twitter_file = "data/twitter.csv";
	others_file = "data/others.csv";
}

//loading
d3.select("#wait")
	.append("svg")
	.attr("width",w)
	.attr("height",h)
	.append("text")
	.attr({
		x:w/2,
		y:h/2,
		fill:"black",
		"font-size":20
	})
	.text("Loading...");


//wordClowd(I_file,"I");
wordClowd(twitter_file,"twitter");
//wordClowd(others_file,"others");

//クラウド処理
function wordClowd(file,param){

	d3.text(file, function(error, text) {

		console.log(file);

	    var data = d3.csv.parseRows(text, function(d) {
	        return { count: d[0], word: d[1]};
	    });

	    //TODO 頻度を割合に変換

		data = data.splice(0, 200); //処理wordを1200件に絞る

		console.log(data);

		var random = d3.random.irwinHall(2)

		//なぞのバグ
		//var countMax = d3.max(data, function(d){ return d.count} );
		var countMax = data[0].count;
		//console.log(countMax);
		var sizeScale = d3.scale.linear()
							.domain([0, countMax])
							.range([1, 200]);
		var colorScale = d3.scale.category20();

		var words = data.map(function(d) {
			return {
			text: d.word,
			size: sizeScale(d.count) //頻出カウントを文字サイズに反映
			};
		});

		d3.layout.cloud().size([w, h])
			.words(words)
			.rotate(function() { return Math.round(1-random()) *90; }) //ランダムに文字を90度回転
			.font("Impact")
			.fontSize(function(d) { return d.size; })
			.on("end", draw) //描画関数の読み込み
			.start();

		//wordcloud 描画
		function draw(words) {

			d3.select("#wait")
				.remove();

			d3.select("#"+param)
			.attr("width",w)
			.attr("height",h)
			.append("g")
			.attr("transform", "translate(0,0)")
			.selectAll("text")
			.data(words)
			.enter()
			.append("text")
			.style({
				"font-family": "Impact",
				"font-size":function(d) { return d.size + "px"; },
				"fill": function(d, i) { return colorScale(i); }
			})
			.attr({
				"text-anchor":"middle",
				"transform": function(d) {
					return "translate(" + [w/2+d.x, h/2+d.y] + ")rotate(" + d.rotate + ")";
				}
			})
			.text(function(d) { return d.text; });
		}

});


}

//URLからGETパラメタを取得
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i <hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}