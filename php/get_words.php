<?php

//ini_set('display_errors', 1);

/* -------------------------------------------------------------------------------------
 * Twitter
------------------------------------------------------------------------------------- */

require_once (dirname(__FILE__).'/tw_api.php');
require_once (dirname(__FILE__).'/mecab.php');

$my_timeline = null;

//パラメータ入力があるとき
$user = $_GET["user"];
//単語リスト
$words = array();

if($user == '')
{
	$log = ("data/twitter_log/kupoyuki.txt");
}
else
{
	$log = ("data/twitter_log/".$user.".txt");
}

//ログファイルがあればスルー
if(!file_exists($log)){

	//取ってこれるだけ取ってくる
	$my_timeline = getUserTimeline($user);

	//ツイートを取得、パラメータ入力がないときは自分のTL
	foreach($my_timeline as $key => $tweet)
	{

		$text = $my_timeline[$key]->text;
		$text = removeNeedlessText($text, $tweet);
		// 形態素解析（mecab）
		$features = getWordFeatures($text);

		//品詞を判断する
		foreach ($features as $key => $value)
		{
			//var_dump($features);
			$feature = $features[$key]["feature"];
			$check = checkWordClass($feature);

			if($check)
			{
				$words[] = $features[$key]["word"];
			}
		}
	}
	file_put_contents($log, serialize($words));
}
else
{
	$words = unserialize(file_get_contents($log));
}

csvOutput($words,"twitter");

/* -------------------------------------------------------------------------------------
 * IandOthers
------------------------------------------------------------------------------------- */

$I = array();
$others = array();

//ログファイルの選択
if($user == '')
{
	$log_I = ("data/I/kupoyuki.txt");
	$log_other = ("data/others/kupoyuki.txt");
}
else
{
	$log_I = ("data/I/".$user.".txt");
	$log_other = ("data/others/".$user.".txt");
}

if(!file_exists($log_I))
{
	//ログファイルの生成
}
else
{
	// $I = unserialize(file_get_contents($log));
	// $others = unserialize(file_get_contents($log));
}

$I = file_get_contents($log_I);
$I = explode( "\n", $I );//分割して配列に

$others = file_get_contents($log_other);
$others = explode( "\n", $others );

csvOutput($I,"I");
csvOutput($others,"others");


/* -------------------------------------------------------------------------------------
 * CSVファイルの生成
------------------------------------------------------------------------------------- */

//$words = array , $prm = twitter,I,others
function csvOutput($words,$param){

	//重複カウント
	$words = array_count_values($words);

	//記号を削除
	foreach ($words as $key => $value) {
		//記号が含まれる→記号以外が含まれない
		if(checkSymbol($key))
		{
			if(!checkAlfNumKanaKanji($key))
			{
				unset($words[$key]);
			}
		}
		//ゴミ除去
		if(checkGomi($key)){
			unset($words[$key]);
		}

	}

	array_values($words);//空きをつめる
	arsort($words);

	$output = '';

	//1 行ずつ CSV に変換してファイルに出力します
	foreach($words as $key => $value){
		$output .= $value.','.$key."\n";
	}

	file_put_contents("data/".$param.".csv", $output);
}

//var_dump($output);

?>