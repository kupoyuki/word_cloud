<?php

/* -------------------------------------------------------------------------------------
 * mecabる
------------------------------------------------------------------------------------- */
function getWordFeatures($text)
{
	$url = 'http://kupoyuki.com/mecab_api/basic.php';
	$data = array(
	    'text' => $text,
	);
	$data = http_build_query($data);

	$header = array(
        "Content-Type: application/x-www-form-urlencoded",
        "Content-Length: ".strlen($data)
    );
	$options = array('http' => array(
	    'method' => 'POST',
	    'header' => implode("\r\n", $header),
	    'content' => $data
	));
	$features = file_get_contents($url, false, stream_context_create($options));
	$features = json_decode($features, true);
	return $features;
}

/* -------------------------------------------------------------------------------------
 * 指定した単語が使用する品詞かどうかを返す
------------------------------------------------------------------------------------- */
function checkWordClass($feature)
{
	$feature = explode(",", $feature);

	// 品詞チェック
	if (in_array($feature[0], array('名詞')))
	{
		if (in_array($feature[1], array('サ変接続','固有名詞','一般')))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

/* -------------------------------------------------------------------------------------
 * 記号チェック
------------------------------------------------------------------------------------- */

//記号含む
function checkSymbol($text){
    return (preg_match('/[^a-zA-Z0-9ぁ-ん一-龠ァ-ヴー]/u', $text));
}

//かな漢字アルファベット含む
function checkAlfNumKanaKanji($text){
    return (preg_match('/[a-zA-Z0-9ぁ-ん一-龠ァ-ヴー]/u', $text));
}

function checkGomi($text){
	//のばし棒だけ
	if(preg_match('/[-ーー]/u', $text))
	{
		if(!preg_match('/[a-zA-Z0-9ぁ-ん一-龠ァ-ヴ]/u', $text))
		{
			return true;
		}
	}

	//httpとwww
	if(preg_match("/http/u", $text)||preg_match("/htt/u", $text)||preg_match("/www/u", $text))
	{
		return true;
	}

	//一文字のかな・カナ・アルファベットを削除
	if(preg_match('/[a-zA-Z0-9ぁ-んァ-ヴー]/u', $text))
	{
		if(mb_strlen($text,"UTF-8") == 1)
		{
			return true;
		}
	}
}

?>