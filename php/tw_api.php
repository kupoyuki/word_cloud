<?php

require_once (dirname(__FILE__).'/tw_oauth/twitteroauth.php');

$consumer_key = "AVNEG0HX3Vhpw7NawYj7ellwK";
$consumer_secret = "XVQUYJzdXqiqeXAziRZiZzmgNmvkqKLDwDchPDtkB3EEdcNueB";
$access_token = "19062623-ojOdNvBOPQMH4xgZweDX2Q5UGBkdJUiCFR0QCWeKk";
$access_token_secret = "beZ44DCeRbSrd5ePjULnSEpUfuoZ9CAoUJbf7SU0H0aBq";


/* -------------------------------------------------------------------------------------
 * ユーザタイムラインを返す
------------------------------------------------------------------------------------- */
function getUserTimeline($screen_name)
{
	global $consumer_key;
	global $consumer_secret;
	global $access_token;
	global $access_token_secret;

	$tw_obj = new TwitterOAuth(
		$consumer_key,
		$consumer_secret,
		$access_token,
		$access_token_secret);

    $statuses = array(); //すべてのツイート用の配列

    for($i = 0; $i < 1600; $i += 200) {

        $param = array('screen_name' => $screen_name, 'count' => '200'); //200件取得
        if(isset($max_id)) {
            $param['max_id'] = $max_id;
        }

        try {
            $tmp_statuses = $tw_obj->get('statuses/user_timeline', $param);
        } catch (TwistException $e) {
            $error = $e->getMessage();
        }

        if(count($tmp_statuses) == 0) { //取得件数0だったら終了
            break;
        } else { //取得したツイートがあった時
            $statuses =  array_merge($statuses, $tmp_statuses); //配列の最後に追加
            $max_id = $tmp_statuses[count($tmp_statuses)-1]->id - 1; //取得した最古のツイートのidを保存
        }
    }

	return $statuses;
}

/* -------------------------------------------------------------------------------------
 * 形態素解析に不要な文字列を除去して返す
------------------------------------------------------------------------------------- */
function removeNeedlessText($tweet_text, $tweet_data)
{
	//リプライネームの削除
    if(!is_null($tweet_data->entities->user_mentions))
    {
        foreach ($tweet_data->entities->user_mentions as $user)
        {
        	// echo "user: ".$user->screen_name."<br>";
            $tweet_text = str_replace("@".$user->screen_name, "", $tweet_text);
        }
    }
    // URL除去
    if (!empty($tweet_data->entities->urls))
    {
        foreach ($tweet_data->entities->urls as $url)
        {
        	// echo "url: ".$url."<br>";
            $tweet_text = str_replace($url->url, "", $tweet_text);
        }
    }
    // メディア除去
    if (!empty($tweet_data->entities->media))
    {
        foreach ($tweet_data->entities->media as $media)
        {
            $media_url = $media->url;
            // echo "media_url: ".$media_url."<br>";
            $tweet_text = str_replace($media_url, "", $tweet_text);
        }
    }
    // ハッシュタグ除去
    if (!empty($tweet_data->entities->hashtags))
    {
        foreach ($tweet_data->entities->hashtags as $hash)
        {
            $hashtag = "#".$hash->text;
            // echo "hashtag: ".$hashtag."<br>";
            $tweet_text = str_replace($hashtag, "", $tweet_text);
        }
    }

    // RTの文字列削除
    $tweet_text = str_replace("RT : ", "", $tweet_text);

    return $tweet_text;
}

?>