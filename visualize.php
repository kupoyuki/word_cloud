<html>
	<head>
		<meta charset="utf-8">
		<script src="js/d3-master/d3.min.js"></script>
    	<script src="js/jquery-2.1.1.min.js"></script>
    	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script> -->
		<script src="js/d3-cloud-master/d3.layout.cloud.js"></script>
	</head>
	<body>
	<?php require_once('php/get_words.php'); ?>
	<div id="wait"></div>
	<div class="result"><svg id="I"></svg></div>
	<div class="result"><svg id="twitter"></svg></div>
	<div class="result"><svg id="others"></svg></div>
	<script src="js/d3.js"></script>
	</body>
</html>